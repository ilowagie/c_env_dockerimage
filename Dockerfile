FROM debian:latest

RUN apt-get update && apt-get install -y software-properties-common
RUN DEBIAN_FRONTEND="noninteractive" apt-get update && apt-get install --fix-missing -y build-essential clang bison flex git pkg-config python3 ninja-build valgrind

# installing gcovr for coverage
RUN apt-get update && apt-get install -y gcovr

# # installing meson build system (more up-to-date? Ubuntu's package doesn't seem to recognize compile -C dir)
RUN apt-get install -y python3-pip && pip3 install --break-system-packages meson
# RUN apt-get update && apt-get install -y meson

# install documentation generation tools
RUN apt-get update && apt-get install -y sphinx doxygen python3-breathe python3-sphinx-rtd-theme

# install gdb (use for generating coredumps in containers)
RUN apt-get update && apt-get install -y gdb && apt-get install -y gdbserver

# installing criterion
RUN apt-get install -y libcriterion-dev

# installing dependencies
RUN apt-get install -y libxxhash-dev
RUN apt-get install -y libjson-c-dev

# installing code formatting tool
RUN apt-get update && apt-get install -y clang-format

# and that's all
