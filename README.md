# Docker container for a C environment

Container for running gitlab CI. Build on the debian:latest container.

## Generating
- Sphinx
- Doxygen
- Breathe
- flex
- bison

## Building
- ninja
- meson

## Libaries
- libxxhash
- json-c

## Testing
- criterion
- GDB
- gcovr

## Misc
- clang-format
